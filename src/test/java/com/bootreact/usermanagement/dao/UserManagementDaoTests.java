package com.bootreact.usermanagement.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.bootreact.usermanagement.model.User;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserManagementDaoTests {

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private UserDaoImpl userDao;
	
	@Test
	public void whenGetByUsername_thenReturnUser() {
		User ali = new User("Ali");
		entityManager.persistAndFlush(ali);
		
		User found = userDao.getByUsername(ali.getUsername());
		assertThat(found.getUsername()).isEqualTo(ali.getUsername());
	}
	
	@Test
	public void whenInvalidName_thenReturnNull() {
		User fromDb = userDao.getByUsername("doesNoExist");
		assertThat(fromDb).isNull();
	}
	
	@Test
	public void whenGetById_thenReturnUser() {
		User user = new User("veli");
		entityManager.persistAndFlush(user);
		
		User fromDb = userDao.getById(user.getId());
		assertThat(fromDb.getUsername()).isEqualTo(user.getUsername());
	}
	
	@Test
	public void givenSetOfUsers_whenFindAll_thenReturnAllUsers() {
		User umay = new User("Umay");
		User sibel = new User("Sibel");
		User ida = new User("Ida");
		
		entityManager.persist(umay);
		entityManager.persist(sibel);
		entityManager.persist(ida);
		entityManager.flush();
		
		List<User> allUsers = userDao.findAllUsers();
		
		assertThat(allUsers).hasSize(3)
			.extracting(User::getUsername)
			.containsOnly(umay.getUsername(), sibel.getUsername(), ida.getUsername());
	}
}
