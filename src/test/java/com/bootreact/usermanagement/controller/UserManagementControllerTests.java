package com.bootreact.usermanagement.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.bootreact.usermanagement.controller.Controller;
import com.bootreact.usermanagement.model.User;
import com.bootreact.usermanagement.service.UserService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@WebMvcTest(Controller.class)
public class UserManagementControllerTests {

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private UserService userService;
	
	@Before
	public void setUp() {
	}
	
	@Test
	public void whenPostUser_thenCreateUser() throws Exception {
		User mehmet = new User("Mehmet");
		//given(userService.createUser(Mockito.anyObject())).willReturn(mehmet);
		
		mvc.perform(post("/api/employees").contentType(MediaType.APPLICATION_JSON)
            .content(toJson(mehmet)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.name", is("Mehmet")));
        verify(userService, VerificationModeFactory.times(1)).createUser(Mockito.anyObject());
        reset(userService);
	}
	
	@Test
	public void givenUsers_whenGetUsers_thenReturnJsonArray() throws Exception {
		User umay = new User("Umay");
		User sibel = new User("Sibel");
		User ida = new User("Ida");
		
		List<User> allUsers = Arrays.asList(umay, sibel, ida);
		given(userService.findAllUsers()).willReturn(allUsers);
		
		mvc.perform(get("/api/employees").contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk())
	        .andExpect(jsonPath("$", hasSize(3)))
	        .andExpect(jsonPath("$[0].name", is(umay.getUsername())))
	        .andExpect(jsonPath("$[1].name", is(sibel.getUsername())))
	        .andExpect(jsonPath("$[2].name", is(ida.getUsername())));
	    verify(userService, VerificationModeFactory.times(1)).findAllUsers();
	    reset(userService);
	}
	
	private static byte[] toJson(Object object) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
	}
}
