package com.bootreact.usermanagement.controller;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.bootreact.usermanagement.UserManagementApplication;
import com.bootreact.usermanagement.dao.UserDaoImpl;
import com.bootreact.usermanagement.model.User;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonInclude;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT, classes = UserManagementApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class UserManagementRestControllerTests {

	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private UserDaoImpl userDao;
	
	@Test
	public void whenValidInput_thenCreateUser() throws IOException, Exception {
		User mete = new User("Mete");
		mvc.perform(post("/api/user").contentType(MediaType.APPLICATION_JSON)
			.content(toJson(mete)));
		
		List<User> found = userDao.findAllUsers();
		assertThat(found).extracting(User::getUsername)
			.containsOnly("Mete");
	}
	
	@Test
	public void givenUsers_whenFindUsers_thenStatusOK() throws Exception {
		createTestUser("Cimri");
		createTestUser("Comert");
		
		mvc.perform(get("/api/users").contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(2))))
        .andExpect(jsonPath("$[0].name", is("bob")))
        .andExpect(jsonPath("$[1].name", is("alex")));
	}
	
	private void createTestUser(String username) {
		User user = new User(username);
		entityManager.persistAndFlush(user);
	}
	
	private static byte[] toJson(Object object) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
	}
}
