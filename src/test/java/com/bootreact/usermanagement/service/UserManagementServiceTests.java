package com.bootreact.usermanagement.service;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.bootreact.usermanagement.dao.UserDaoImpl;
import com.bootreact.usermanagement.model.User;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class UserManagementServiceTests {

	static class UserServiceImplTestContextConfiguration {
		@Bean
		public UserService userService() {
			return new UserServiceImpl();
		}
	}
	
	@Autowired
	private UserService userService;
	
	@MockBean
	private UserDaoImpl userDao;
	
	@Before
	public void setUp() {
		User umay = new User("Umay");
		umay.setId(11L);
		
		User sibel = new User("Sibel");
		User ida = new User("Ida");
		
		List<User> allUsers = Arrays.asList(umay, sibel, ida);
		
		Mockito.when(userDao.getByUsername(umay.getUsername()))
			.thenReturn(umay);
		Mockito.when(userDao.getByUsername(sibel.getUsername()))
			.thenReturn(sibel);
		Mockito.when(userDao.getByUsername("wrong_name"))
			.thenReturn(null);
		Mockito.when(userDao.getById(umay.getId()))
			.thenReturn(umay);
		Mockito.when(userDao.findAllUsers())
			.thenReturn(allUsers);
		Mockito.when(userDao.getById(-99L))
			.thenReturn(null);
	}
	
	@Test
	public void whenValidName_thenUserShouldBeFound() {
		String name = "Mehmet";
		User found = userService.getByUsername(name);
		
		assertThat(found.getUsername()).isEqualTo(name);
	}
	
	@Test
	public void whenInvalidName_thenReturnShouldBeNull() {
		User fromDb = userService.getByUsername("wrong_name");
		assertThat(fromDb).isNull();
		
		verifyFindByNameIsCalledOnce("wrong_name");
	}
	
	@Test
	public void whenValidId_thenUserShouldBeFound() {
		User fromDb = userService.getUserById(11L);
		assertThat(fromDb).isNotNull();
		
		verifyFindByIdIsCalledOnce();
	}
	
	@Test
	public void given3Users_whenGetAll_thenReturn3Records() {
		User umay = new User("Umay");
		User sibel = new User("Sibel");
		User ida = new User("Ida");
		
		List<User> allUsers = userService.findAllUsers();
		
		verifyFindAllEmployeesIsCalledOnce();
		
		assertThat(allUsers).hasSize(3)
			.extracting(User::getUsername)
			.contains(umay.getUsername(), sibel.getUsername(), ida.getUsername());
	}
	
	@Test
	public void whenCreateNewUser_thenNewUserShouldBeFound() {
		userService.createUser(new User("fill-labs"));
		User newUser = userService.getByUsername("fill-labs");
		assertThat(newUser).isNotNull();
	}
	
	@Test
	public void whenUpdateUser_thenUserShouldBeUpdated() {
		User userToBeUpdated = new User("Mehmet");
		boolean isUserExist = userService.isUserExist(userToBeUpdated);
		assertThat(isUserExist).isEqualTo(true);
		
		userToBeUpdated.setUsername("Mahmut");
		userService.updateUser(userToBeUpdated);
		User fromDb = userService.getByUsername(userToBeUpdated.getUsername());
		assertThat(userToBeUpdated.getId()).isEqualTo(fromDb.getId());
	}
	
	private void verifyFindByNameIsCalledOnce(String name) {
        Mockito.verify(userDao, VerificationModeFactory.times(1))
            .getByUsername(name);
        Mockito.reset(userDao);
    }

    private void verifyFindByIdIsCalledOnce() {
        Mockito.verify(userDao, VerificationModeFactory.times(1))
            .getById(Mockito.anyLong());
        Mockito.reset(userDao);
    }

    private void verifyFindAllEmployeesIsCalledOnce() {
        Mockito.verify(userDao, VerificationModeFactory.times(1))
            .findAllUsers();
        Mockito.reset(userDao);
    }
}
