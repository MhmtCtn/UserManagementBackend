package com.bootreact.usermanagement.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.bootreact.usermanagement.model.BaseEntity;

public interface BaseDao<T extends BaseEntity> extends Serializable {
	
	/**
	 * Persists the entity
	 * @param entity
	 */
	public void create(T entity);

	/**
	 * Merges the entity
	 * @param entity
	 */
	public void update(T entity);

	/**
	 * Removes the entity
	 * @param entity
	 */
	public void delete(T entity);

	/**
	 * Returns an entity with the given id
	 * @param id
	 * @return
	 */
	public T getById(long id);
}
