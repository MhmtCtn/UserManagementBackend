package com.bootreact.usermanagement.dao;

import java.util.List;

import com.bootreact.usermanagement.model.User;

public interface UserDao {

	/**
	 * Returns user by username
	 * @param username
	 * @return
	 */
	User getByUsername(String username);
	
	/**
	 * Returns all users
	 * @return
	 */
	List<User> findAllUsers();
	
	/**
	 * Creates a new user
	 * @param user
	 */
	void createUser(User user);
	
	/**
	 * Updates a user
	 * @param user
	 */
	void updateUser(User user);
	
	/**
	 * Deletes a user
	 * @param user
	 */
	void deleteUser(User user);
}
