package com.bootreact.usermanagement.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bootreact.usermanagement.model.User;

@Repository
@Transactional
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {

	public User getByUsername(String username) {
		Query query = entityManager.createQuery("select r from User r where username = :username");
		query.setParameter("username", username);
		return (User) query.getSingleResult();
	}

	@Override
	public List<User> findAllUsers() {
		Query query = entityManager.createQuery("from User");
		return query.getResultList();
	}

	@Override
	public void createUser(User user) {
		create(user);
	}

	@Override
	public void updateUser(User user) {
		update(user);
	}

	@Override
	public void deleteUser(User user) {
		delete(user);
	}
}
