package com.bootreact.usermanagement.dao;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bootreact.usermanagement.model.BaseEntity;

@Transactional
@Repository
public abstract class BaseDaoImpl<T extends BaseEntity> implements BaseDao<T> {

	@PersistenceContext
	protected EntityManager entityManager;

	private final Class<T> entityClass;
	
	public BaseDaoImpl() {
		this.entityClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	public void create(T entity) {
		entityManager.persist(entity);
	}

	public void update(T entity) {
		entityManager.merge(entity);
	}

	public void delete(T entity) {
		entityManager.remove(entity);
	}

	public T getById(long id) {
		return entityManager.find(entityClass, id);
	}
}
