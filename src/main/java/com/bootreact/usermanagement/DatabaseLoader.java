package com.bootreact.usermanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.bootreact.usermanagement.dao.UserDaoImpl;
import com.bootreact.usermanagement.model.User;
import com.bootreact.usermanagement.service.UserServiceImpl;

@Component
public class DatabaseLoader implements CommandLineRunner {

	private final UserServiceImpl userDaoImpl;

    @Autowired
    public DatabaseLoader(UserServiceImpl userDaoImpl) {
        this.userDaoImpl = userDaoImpl;
    }

    @Override
    public void run(String... strings) throws Exception {
        this.userDaoImpl.createUser(new User("Mehmet"));
        this.userDaoImpl.createUser(new User("Mustafa"));
    }
}
