package com.bootreact.usermanagement.service;

import java.util.List;

import com.bootreact.usermanagement.model.User;

public interface UserService {
	
	/**
	 * Returns user by id
	 * @param id
	 * @return
	 */
	User getUserById(long id);
	
	/**
	 * Returns user by username
	 * @param username
	 * @return
	 */
	User getByUsername(String username);
	
	/**
	 * Returns all users
	 * @return
	 */
	List<User> findAllUsers();
	
	/**
	 * Creates a new user
	 * @param user
	 */
	void createUser(User user);
	
	/**
	 * Updates a user
	 * @param user
	 */
	void updateUser(User user);
	
	/**
	 * Deletes a user
	 * @param user
	 */
	void deleteUser(User user);
	
	/**
	 * Checks if the user is exist
	 * @param user
	 * @return
	 */
	boolean isUserExist(User user);
}
