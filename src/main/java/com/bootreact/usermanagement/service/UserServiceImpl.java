package com.bootreact.usermanagement.service;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bootreact.usermanagement.dao.UserDao;
import com.bootreact.usermanagement.dao.UserDaoImpl;
import com.bootreact.usermanagement.model.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	UserDaoImpl userDao;

	@Override
	public User getUserById(long id) {
		return userDao.getById(id);
	}

	@Override
	public User getByUsername(String username) {
		return userDao.getByUsername(username);
	}

	@Override
	public List<User> findAllUsers() {
		return userDao.findAllUsers();
	}

	@Override
	public void createUser(User user) {
		user.setCreatedAt(Calendar.getInstance());
		userDao.createUser(user);
	}

	@Override
	public void updateUser(User user) {
		User entity = userDao.getById(user.getId());
		if(entity != null) {
			entity.setUsername(user.getUsername());
			entity.setUpdatedAt(Calendar.getInstance());
		}
	}

	@Override
	public void deleteUser(User user) {
		userDao.deleteUser(user);
	}

	@Override
	public boolean isUserExist(User user) {
		User isExist = getByUsername(user.getUsername());
		if(isExist != null)
			return true;
		return false;
	}

}
