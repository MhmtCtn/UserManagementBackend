package com.bootreact.usermanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.bootreact.usermanagement.model.User;
import com.bootreact.usermanagement.service.UserService;

@RestController
@RequestMapping("/api")
public class Controller {

	@Autowired
    private UserService userService;

	/**
	 * Rest method to return all users
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/users")
	public ResponseEntity<List<User>> findAllUsers() {
		List<User> users = userService.findAllUsers();
		if (users.isEmpty()) {
			return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
	    }
	    return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	/**
	 * Rest method to return a user with the given id
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/users/{id}")
	public ResponseEntity<User> getUser(@PathVariable(value = "id") Long id) {
		User user = userService.getUserById(id);
		if(user == null) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}
	}
	/**
	 * Rest method to create a new user with the given entity
	 * @param user
	 * @param ucBuilder
	 * @return
	 */
	@RequestMapping(value = "/user", method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody User user, UriComponentsBuilder ucBuilder) {
		System.out.println("Creating User " + user.getUsername());
		if (userService.isUserExist(user)) {
		  System.out.println("A User with name " + user.getUsername() + " already exist");
		  return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		userService.createUser(user);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	/**
	 * Rest method to update a user with the given id and an entity that attributes taken from
	 * @param id
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
	public ResponseEntity<User> updateUser(@PathVariable("id") long id, @RequestBody User user) {
	    System.out.println("Updating User " + id);
	    User currentUser = userService.getUserById(id);
	    if (currentUser == null) {
	      System.out.println("User with id " + id + " not found");
	      return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	    }
	    currentUser.setUsername(user.getUsername());
	    userService.updateUser(currentUser);
	    return new ResponseEntity<User>(currentUser, HttpStatus.OK);
	}
	/**
	 * Rest method to delete a user with the given id
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<User> deleteUser(@PathVariable("id") long id) {
	    System.out.println("Fetching & Deleting User with id " + id);
	    User user = userService.getUserById(id);
	    if (user == null) {
	      System.out.println("Unable to delete. User with id " + id + " not found");
	      return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	    }
	    userService.deleteUser(user);
	    return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
	}
}
