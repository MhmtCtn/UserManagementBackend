A Maven based REST project that uses Spring Boot and Hibernate JPA.

Pre-Loading the demo

To work with this application, you need to pre-load it with some data like that
in the DatabaseLoader class.
This class is marked with Spring’s @Component annotation so that it is automatically picked up by @SpringBootApplication.
It implements Spring Boot’s CommandLineRunner so that it gets run after all the beans are created and registered.
It uses constructor injection and autowiring to reach the User Repository.
The run() method is invoked with command line arguments, loading up the data.

Launching the backend

To launch the project run the main() method inside your IDE, or type ./mvnw spring-boot:run
on the command line. (mvnw.bat for Windows users).

Then you can reach it at localhost:8080/api on any browser or by typing curl localhost:8080/api
on the command line.
